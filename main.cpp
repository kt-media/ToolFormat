#include <iostream>

#include "flv/flv.hpp"
#include "mp4/mp4-dump.h"

NS_USING


int main(int argc, char *argv[])
{
    int result = 0;

    // mp4 dump
    result = mp4_dump_main(argc, argv);

    return 0;
}

