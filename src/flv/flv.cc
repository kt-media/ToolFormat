#include "flv/flv.hpp"
#include "basic/amf0.h"


// 相关格式学习地址
// https://blog.csdn.net/beyond702/article/details/78929334
// https://www.cnblogs.com/poissonnotes/p/7744283.html
// https://www.cnblogs.com/leisure_chn/p/10662941.html
// https://github.com/imagora/FlvParser
// a good code:
// https://github.com/noirotm/flvmeta



#include <iostream>
#include <cstdio>
#include <memory>

NS_BEGIN

const char *audio_format_string(u8 format)
{
    switch (format) {
        case AUDIO_FORMAT_LINEAR_PCM_PE:
            return "Linear PCM, platform endian";
        case AUDIO_FORMAT_ADPCM:
            return "ADPCM";
        case AUDIO_FORMAT_MP3:
            return "MP3";
        case AUDIO_FORMAT_LINEAR_PCM_LE:
            return "Linear PCM, little endian";
        case AUDIO_FORMAT_NELLYMOSER_16K:
            return "Nellymoser 16-kHz mono";
        case AUDIO_FORMAT_NELLYMOSER_8K:
            return "Nellymoser 8-kHz mono";
        case AUDIO_FORMAT_NELLYMOSER:
            return "Nellymoser";
        case AUDIO_FORMAT_L_PCM_G711_LAW_A:
            return "G.711 A-law  logarithmic PCM";
        case AUDIO_FORMAT_L_PCM_G711_LAW_MU:
            return "G.711 mu-law logarithmic PCM";
        case AUDIO_FORMAT_RESERVED:
            return "reserved";
        case AUDIO_FORMAT_AAC:
            return "AAC";
        case AUDIO_FORMAT_SPEEX:
            return "speex";
        case AUDIO_FORMAT_MP3_8K:
            return "MP3 8-Khz";
        case AUDIO_FORMAT_DEVICE_S_S:
            return "Device-specific sound";
        default:
            return "unknown audio format";
    }
}

const char *audio_rate_string(u8 rate)
{
    switch (rate) {
        case AUDIO_RATE_KHZ_5_5:
            return "5.5Khz";
        case AUDIO_RATE_KHZ_11:
            return "11Khz";
        case AUDIO_RATE_KHZ_22:
            return "22Khz";
        case AUDIO_RATE_KHZ_44:
            return "44Khz";
        default:
            return "unknown rate";
    }
}

const char *audio_size_string(u8 size)
{
    switch (size) {
        case AUDIO_SIZE_SAN_8_BIT:
            return "sand8bit";
        case AUDIO_SIZE_SAN_16_BIT:
            return "sand16bit";
        default:
            return "unknown bit";
    }
}

const char *audio_type_string(u8 type)
{
    switch (type) {
        case AUDIO_TYPE_SND_MONO:
            return "sanMono";
        case AUDIO_TYPE_SND_STEREO:
            return "sanStereo";
        default:
            return "unknown audio type";
    }
}


const char *video_frame_type_string(u8 type)
{
    switch (type) {
        case VIDEO_FRAME_TYPE_KEY_FRAME:
            return "key frame (for AVC, a seekable frame)";
        case VIDEO_FRAME_TYPE_INTER_FRAME:
            return "inter frame (for AVC, a non-seekable frame)";
        case VIDEO_FRAME_TYPE_263_ONLY:
            return "disposable inter frame (H.263 only)";
        case VIDEO_FRAME_TYPE_GENERATED_KEY_F:
            return "generated key frame (reserved for server use only)";
        case VIDEO_FRAME_TYPE_COMMON_FRAME:
            return "video info/command frame";
        default:
            return "unknown video frame type";
    }
}

const char *video_codec_identifier_string(u8 codec_id)
{
    switch (codec_id) {
        case VIDEO_CODEC_ID_263:
            return "Sorenson H.263";
        case VIDEO_CODEC_ID_SCREEN_VIDEO:
            return "Screen video";
        case VIDEO_CODEC_ID_VP6:
            return "On2 VP6";
        case VIDEO_CODEC_ID_VP6_ALPHA:
            return "On2 VP6 with alpha channel";
        case VIDEO_CODEC_ID_SCREEN_VIDEO_V2:
            return "Screen video version 2";
        case VIDEO_CODEC_ID_AVC:
            return "AVC";
        default:
            return "unknown video codec identifier";
    }
}

const char *video_avc_packet_type_string(u8 type)
{
    switch (type) {
        case AVC_SEQUENCE_HEADER:
            return "AVC sequence header";
        case AVC_NALU:
            return "AVC NALU";
        case AVC_END_SEQUENCE:
            return "AVC end of sequence";
        default:
            return "unknown avc packet type";
    }
}




uint8 FLVHeader::readHeader(FILE *file)
{
    if(nullptr == file) return FLV_PARSE_NULL_POINTER;

    if (fread(signature, sizeof(signature),   1, file) == 0 ||
        fread(&version,  sizeof(version), 1, file) == 0 ||
        fread(&flags,    sizeof(flags),   1, file) == 0 ||
        fread(&length,   sizeof(length),  1, file) == 0) {
        return FLV_PARSE_EOF;
    }

    if(signature[0] != 'F' && signature[1] != 'L' && signature[2] != 'V')
        return FLV_PARSE_NOT_FLV_FILE;

    uint32 headLength = getLength();
    if(headLength < FLV_HEAD_DEFAULT_LEN)
        return FLV_HEAD_DEFAULT_LEN;

    if(headLength > FLV_HEAD_DEFAULT_LEN) {
        extra_len = headLength - FLV_HEAD_DEFAULT_LEN;
        if(extra != nullptr) {
            v8 *ptr = extra.release();
            ptr->clear();

            extra = std::make_unique<v8>(extra_len);
        }

        if( fread(extra->data(), sizeof(byte), extra_len, file) != extra_len) {
            return FLV_PARSE_NOT_FLV_FILE;
        }
    }
    return FLV_PARSE_OK;
}

uint8 FLVHeader::parseFLVHeadExtra(onHeadExtra cb, void *out)
{
    if( extra == nullptr || extra->empty() || extra_len <= 0 || cb == nullptr)
        return FLV_PARSE_OK;
    return cb(out, *extra);
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//


uint8 FLVTagHeader::readTagHeader(FILE *file)
{
    if(nullptr == file) return FLV_PARSE_NULL_POINTER;

    // read the flv tag basic information
    if(fread(&tag_type,           sizeof(tag_type),           1, file) == 0 ||
       fread(&body_length,        sizeof(body_length),        1, file) == 0 ||
       fread(&timestamp,          sizeof(timestamp),          1, file) == 0 ||
       fread(&timestamp_extended, sizeof(timestamp_extended), 1, file) == 0 ||
       fread(&stream_id,          sizeof(stream_id),          1, file) == 0) {
        return FLV_PARSE_EOF;
    }
    return FLV_PARSE_OK;
}

uint8 FLVTagHeader::readBasicTagType(FILE *file, BasicTagType *tagType)
{
    if(nullptr == file || nullptr == tagType)
        return FLV_PARSE_NULL_POINTER;

    if ( fread(&tagType, sizeof(BasicTagType), 1, file) == 0 )
        return FLV_PARSE_EOF;
    return FLV_PARSE_OK;
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//

FLVTagAudio::FLVTagAudio(FLVTagHeader& header): tagHeader{header}
{
}

uint8 FLVTagAudio::parseAudioTag(FILE *file)
{
    if(nullptr == file) return FLV_PARSE_NULL_POINTER;

    length = tagHeader.getBodyLength();
    if(body != nullptr) {
        v8 *ptr = body.release();
        ptr->clear();
    }
    body = std::make_unique<v8>(length);
    if( length != fread(body->data(), sizeof(byte), length, file) ) {
        return FLV_PARSE_EOF;
    }
    audioType = body->at(0);

    return FLV_PARSE_OK;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//

FLVTagVideo::FLVTagVideo(FLVTagHeader& header): videoType{0u}, tagHeader{header}
{
}

uint8 FLVTagVideo::parseVideoTag(FILE *file)
{
    if(nullptr == file) return FLV_PARSE_NULL_POINTER;

    length = tagHeader.getBodyLength();
    if(body != nullptr) {
        v8 *ptr = body.release();
        ptr->clear();
    }
    body = std::make_unique<v8>(length);
    if( length != fread(body->data(), sizeof(byte), length, file) ) {
        return FLV_PARSE_EOF;
    }
    videoType = body->at(0);

    return FLV_PARSE_OK;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
FLVTagScript::FLVTagScript(FLVTagHeader& header):
    tagHeader(header)
{
}

uint8 FLVTagScript::parseScriptTag(FILE *file)
{
    if (nullptr == file) return FLV_PARSE_NULL_POINTER;

    length = tagHeader.getBodyLength();
    if (body != nullptr) {
        v8 *ptr = body.release();
        ptr->clear();
    }

    body = std::make_unique<v8>(length);
    if (length != fread(body->data(), sizeof(byte), length, file)) {
        return FLV_PARSE_EOF;
    }

    //AMFObject object{};
    //int result = AMF_Decode(&object, reinterpret_cast<const char *>(body->data()), length, 0);

    return FLV_PARSE_OK;
}


bool OnTagParseDefault::onTagScript(v8& body)
{
    if(body.empty()) return false;
    return true;
}

bool OnTagParseDefault::onTagAudio(BasicTagType type, v8& body)
{
    uint32 bodeLength = body.size() - 1;
    uint8 format = FLVTagAudio::audio_format(type);
    uint8 rate   = FLVTagAudio::audio_rate(type);
    uint8 size   = FLVTagAudio::audio_size(type);
    uint8 tp     = FLVTagAudio::audio_type(type);
    printf("| audio: format=%s rate=%s size=%s tp=%s audio-data-length=%d\n",
           audio_format_string(format),
           audio_rate_string(rate),
           audio_size_string(size),
           audio_type_string(tp),
           bodeLength
          );

    // aac 文件格式
    if (format == AUDIO_FORMAT_AAC) {
        u8 aacType = body.at(1);
        if(aacType == 0) {
            printf("| audio: aac sequence header size=%d\n", body.size() - 2);
        }
        else {
            printf("| audio: aac raw size=%d\n", body.size() - 2);
        }
    }
    else { // 其他文件格式
    }

    return true;
}

bool OnTagParseDefault::onTagVideo(BasicTagType type, v8& body)
{
    if (body.empty())return false;

    auto it = body.cbegin();
    if(*it++ != type) return false;

    u8 frameType = FLVTagVideo::frame_type(type);
    u8 codec_id  = FLVTagVideo::codec_identifier(type);
    printf("| video frame type=%s  codec-identifier=%s size=%d\n",
           video_frame_type_string(frameType),
           video_codec_identifier_string(codec_id),
           body.size()
          );

    // 在 FLV 白皮书中介绍 VideoTagHeader
    // CompositionTime 发现都是 0
    // 这个主要是 pts 和 dts 值相同
    if(codec_id == VIDEO_CODEC_ID_AVC) {
        // 读取 AVC pkt type
        u8     pkt_type        = *it;
        int32  compositionTime = 0;
        if(pkt_type == AVC_NALU)
            compositionTime = ntoh_uint24_3(*it++, *it++, *it);
        else
            it += 2;
        printf("|  avc  type=%s  composition-time=%d\n",
               video_avc_packet_type_string(pkt_type),
               compositionTime
              );
    }
    else {
        // 非 AVC 内容
    }


    return true;
}

bool OnTagParseDefault::onStreamEnd()
{
    return true;
}



//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//

uint8 FLVParser::readPreviousTagSize(FILE *file, uint32 *value)
{
    if (nullptr == file || nullptr == value)
        return FLV_PARSE_NULL_POINTER;
    uint32 result = 0;
    if (fread(&result, sizeof(uint32), 1, file) == 0)
        return FLV_PARSE_EOF;
    *value = ntoh_uint32(result);
    return FLV_PARSE_OK;
}


FLVParser::FLVParser(const char *path):
    ptrFilePath(std::make_unique<std::string>(path)),
    hasParsed{false}
{
}

FLVParser::~FLVParser()
{
    closeFile();
}

void FLVParser::openFile()
{
    if(nullptr != ptrFilePath) {
        FILE *file = fopen(ptrFilePath->c_str(), "rb");
        if(nullptr != file) {
            ptrFile.reset(file);
        }
    }
}

void FLVParser::closeFile()
{
    if (nullptr != ptrFile) {
        FILE *file = ptrFile.release();
        if (nullptr != file) {
            fclose(file);
        }
    }
}

uint8 FLVParser::parser(IOnTagParse *listener)
{
    if (hasParsed)
        return FLV_HAD_PARSED;
    hasParsed = true;

    // open flv source file
    openFile();

    FLVHeader header{};
    uint8 result = header.readHeader(ptrFile.get());
    if (result != FLV_PARSE_OK) return result;

    // parse the first
    uint32 previousSize = 0;
    if (FLV_PARSE_OK != readPreviousTagSize(ptrFile.get(), &previousSize))
        return FLV_PARSE_EOF;

    // loop get the tag and previous-size
    while ( true ) {

#define RETURN_EXIT(cond)     \
    do {                      \
        if( (cond) ) {        \
            goto RETURN_EXIT; \
        }                     \
    } while(0)

        if(feof(ptrFile.get())) {
            std::cout << "get the end of the file" << std::endl;
            break;
        }

        FLVTagHeader tagHeader{};
        result = tagHeader.readTagHeader(ptrFile.get());
        if(result == FLV_PARSE_EOF) break;

        RETURN_EXIT( FLV_PARSE_OK != result );

        bool isEncryptedBody = tagHeader.hasPreProcessing();
        uint8 tagType = tagHeader.getTagType();
        printf("++++ type=%d body-length=%d stream-id=%d timestamp=%d is-processing=%s\n",
               tagType, tagHeader.getBodyLength(),
               tagHeader.getStreamId(),
               tagHeader.getTimestamp(),
               isEncryptedBody ? "true" : "false");
        if(isEncryptedBody) {
            std::cout << "this is encrypted body, not supported" << std::endl;
            return FLV_PROCESSING_UNSUPPORTED;
        }

        switch (tagType) {
            case FLV_TAG_TYPE_AUDIO: {
                FLVTagAudio audioTag(tagHeader);
                result = audioTag.parseAudioTag(ptrFile.get());
                RETURN_EXIT( FLV_PARSE_OK != result );

                if(nullptr != listener) listener->onTagAudio(audioTag.audioType, *audioTag.body);

                break;
            }
            case FLV_TAG_TYPE_VIDEO: {
                FLVTagVideo videoTag(tagHeader);
                result = videoTag.parseVideoTag(ptrFile.get());
                RETURN_EXIT( FLV_PARSE_OK != result );

                if(nullptr != listener) listener->onTagVideo(videoTag.videoType, *videoTag.body);

                break;
            }
            case FLV_TAG_TYPE_SCRIPT_DATA: {
                FLVTagScript script(tagHeader);
                result = script.parseScriptTag(ptrFile.get());
                RETURN_EXIT( FLV_PARSE_OK != result );

                if(nullptr != listener) listener->onTagScript(*script.body);

                break;
            }
            default: {
                std::cerr << "unknown tag. type=" << tagType << std::endl;
                RETURN_EXIT( true );
            }
        }

        // read the previous size
        uint32 middlePreviousSize = 0;
        if (FLV_PARSE_OK != readPreviousTagSize(ptrFile.get(), &middlePreviousSize))
            return FLV_PARSE_EOF;

        std::cout << "last tag size=" << middlePreviousSize << std::endl;
    }

RETURN_EXIT:
    if(nullptr != listener) listener->onStreamEnd();

    closeFile();
    return result;
}

NS_END
