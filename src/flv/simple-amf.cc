#include "flv/simple-amf.hpp"

#include <cstring>
#include <ctime>

NS_BEGIN

#ifdef __cplusplus
extern "C" {
#endif


// read a number
static amf_data *amf_number_read(amf_read_proc read_proc, void *user_data)
{
    number64 val;
    if (read_proc(&val, sizeof(number64), user_data) == sizeof(number64)) {
        return amf_number_new(ntoh_number64(val));
    }
    else {
        return amf_data_error(AMF_ERROR_EOF);
    }
}

// read boolean
static amf_data *amf_boolean_read(amf_read_proc read_proc, void *user_data)
{
    uint8 val;
    if (read_proc(&val, sizeof(uint8), user_data) == sizeof(uint8)) {
        return amf_boolean_new(val);
    }
    else {
        return amf_data_error(AMF_ERROR_EOF);
    }
}

// read a string
static amf_data *amf_string_read(amf_read_proc read_proc, void *user_data)
{
    uint16 strsize;
    byte *buffer;

    if (read_proc(&strsize, sizeof(uint16), user_data) < sizeof(uint16)) {
        return amf_data_error(AMF_ERROR_EOF);
    }

    strsize = ntoh_uint16(strsize);
    if (strsize == 0) {
        return amf_string_new(nullptr, 0);
    }

    buffer = (byte *)calloc(strsize, sizeof(byte));
    if (buffer == nullptr) {
        return nullptr;
    }

    if (read_proc(buffer, strsize, user_data) == strsize) {
        amf_data *data = amf_string_new(buffer, strsize);
        free(buffer);
        return data;
    }
    else {
        free(buffer);
        return amf_data_error(AMF_ERROR_EOF);
    }
}


// read an object
static amf_data *amf_object_read(amf_read_proc read_proc, void *user_data)
{
    amf_data *name;
    amf_data *element;
    byte      error_code;
    amf_data *data;

    data = amf_object_new();
    if (data == nullptr)  return nullptr;

    while (true) {
        name       = amf_string_read(read_proc, user_data);
        error_code = amf_data_get_error_code(name);
        if (error_code != AMF_ERROR_OK) {
            // invalid name: error
            amf_data_free(name);
            amf_data_free(data);
            return amf_data_error(error_code);
        }

        element    = amf_data_read(read_proc, user_data);
        error_code = amf_data_get_error_code(element);
        if (error_code == AMF_ERROR_END_TAG || error_code == AMF_ERROR_UNKNOWN_TYPE) {
            // end tag or unknown element: end of data, exit loop
            amf_data_free(name);
            amf_data_free(element);
            break;
        }
        else if (error_code != AMF_ERROR_OK) {
            amf_data_free(name);
            amf_data_free(data);
            amf_data_free(element);
            return amf_data_error(error_code);
        }

        if (amf_object_add(data, (char *)amf_string_get_bytes(name), element) == nullptr) {
            amf_data_free(name);
            amf_data_free(element);
            amf_data_free(data);
            return nullptr;
        }
        else {
            amf_data_free(name);
        }
    }

    return data;
}

// read an associative array
static amf_data *amf_associative_array_read(amf_read_proc read_proc, void *user_data)
{
    amf_data *name;
    amf_data *element;
    uint32    size;
    byte      error_code;
    amf_data *data;

    data = amf_associative_array_new();
    if (data == nullptr) {
        return nullptr;
    }

    // we ignore the 32 bits array size marker
    if (read_proc(&size, sizeof(uint32), user_data) < sizeof(uint32)) {
        amf_data_free(data);
        return amf_data_error(AMF_ERROR_EOF);
    }

    while(true) {
        name       = amf_string_read(read_proc, user_data);
        error_code = amf_data_get_error_code(name);
        if (error_code != AMF_ERROR_OK) {
            // invalid name: error */
            amf_data_free(name);
            amf_data_free(data);
            return amf_data_error(error_code);
        }

        element    = amf_data_read(read_proc, user_data);
        error_code = amf_data_get_error_code(element);
        if (amf_string_get_size(name) == 0 || error_code == AMF_ERROR_END_TAG || error_code == AMF_ERROR_UNKNOWN_TYPE) {
            // end tag or unknown element: end of data, exit loop
            amf_data_free(name);
            amf_data_free(element);
            break;
        }
        else if (error_code != AMF_ERROR_OK) {
            amf_data_free(name);
            amf_data_free(data);
            amf_data_free(element);
            return amf_data_error(error_code);
        }

        if (amf_associative_array_add(data, (char *)amf_string_get_bytes(name), element) == nullptr) {
            amf_data_free(name);
            amf_data_free(element);
            amf_data_free(data);
            return nullptr;
        }
        else {
            amf_data_free(name);
        }
    }
    return data;
}

// read an array
static amf_data *amf_array_read(amf_read_proc read_proc, void *user_data)
{
    size_t    i;
    amf_data *element;
    byte      error_code;
    amf_data *data;
    uint32    array_size;

    data = amf_array_new();
    if (data == nullptr) {
        return nullptr;
    }
    if (read_proc(&array_size, sizeof(uint32), user_data) < sizeof(uint32)) {
        amf_data_free(data);
        return amf_data_error(AMF_ERROR_EOF);
    }

    array_size = ntoh_uint32(array_size);

    for (i = 0; i < array_size; ++i) {
        element    = amf_data_read(read_proc, user_data);
        error_code = amf_data_get_error_code(element);
        if (error_code != AMF_ERROR_OK) {
            amf_data_free(element);
            amf_data_free(data);
            return amf_data_error(error_code);
        }

        if (amf_array_push(data, element) == nullptr) {
            amf_data_free(element);
            amf_data_free(data);
            return nullptr;
        }
    }

    return data;
}

// read a date
static amf_data *amf_date_read(amf_read_proc read_proc, void *user_data)
{
    number64_be milliseconds;
    uint16      timezone;
    if (read_proc(&milliseconds, sizeof(number64_be), user_data) == sizeof(number64_be) &&
        read_proc(&timezone,     sizeof(uint16),      user_data) == sizeof(uint16)) {
        return amf_date_new(ntoh_number64(milliseconds), ntoh_sint16(timezone));
    }
    else {
        return amf_data_error(AMF_ERROR_EOF);
    }
}


// write a number
static size_t amf_number_write(const amf_data *data, amf_write_proc write_proc, void *user_data)
{
    number64 n = ntoh_number64(data->number_data);
    return write_proc(&n, sizeof(number64), user_data);
}

// write a boolean
static size_t amf_boolean_write(const amf_data *data, amf_write_proc write_proc, void *user_data)
{
    return write_proc(&(data->boolean_data), sizeof(uint8), user_data);
}

// write a string
static size_t amf_string_write(const amf_data *data, amf_write_proc write_proc, void *user_data)
{
    uint16 s;
    size_t w = 0;

    s = ntoh_uint16(data->string_data.size);
    w = write_proc(&s, sizeof(uint16), user_data);
    if (data->string_data.size > 0) {
        w += write_proc(data->string_data.mbstr, (size_t)(data->string_data.size), user_data);
    }

    return w;
}

// write an object
static size_t amf_object_write(const amf_data *data, amf_write_proc write_proc, void *user_data)
{
    amf_node *node;
    size_t    w          = 0;
    uint16    filler     = ntoh_uint16(0);
    uint8     terminator = AMF_TYPE_OBJECT_AND_MARKER;

    node = amf_object_first(data);
    while (node != nullptr) {
        w   += amf_string_write(amf_object_get_name(node), write_proc, user_data);
        w   += amf_data_write(amf_object_get_data(node), write_proc, user_data);
        node = amf_object_next(node);
    }

    // empty string is the last element
    w += write_proc(&filler, sizeof(uint16), user_data);
    // an object ends with 0x09
    w += write_proc(&terminator, sizeof(uint8), user_data);

    return w;
}

// write an associative array
static size_t amf_associative_array_write(const amf_data *data, amf_write_proc write_proc, void *user_data)
{
    amf_node *node;
    size_t   w          = 0;
    uint32   s;
    uint16   filler     = ntoh_uint16(0);
    uint8    terminator = AMF_TYPE_OBJECT_AND_MARKER;

    s    = ntoh_uint32(data->list_data.size) / 2;
    w   += write_proc(&s, sizeof(uint32), user_data);
    node = amf_associative_array_first(data);
    while (node != nullptr) {
        w += amf_string_write(amf_associative_array_get_name(node), write_proc, user_data);
        w += amf_data_write  (amf_associative_array_get_data(node), write_proc, user_data);
        node = amf_associative_array_next(node);
    }

    // empty string is the last element
    w += write_proc(&filler, sizeof(uint16), user_data);
    // an object ends with 0x09
    w += write_proc(&terminator, sizeof(uint8), user_data);

    return w;
}

// write an array
static size_t amf_array_write(const amf_data *data, amf_write_proc write_proc, void *user_data)
{
    amf_node *node;
    size_t    w = 0;
    uint32    s;

    s    = ntoh_uint32(data->list_data.size);
    w   += write_proc(&s, sizeof(uint32), user_data);
    node = amf_array_first(data);
    while (node != nullptr) {
        w   += amf_data_write(amf_array_get(node), write_proc, user_data);
        node = amf_array_next(node);
    }

    return w;
}

// write a date
static size_t amf_date_write(const amf_data *data, amf_write_proc write_proc, void *user_data)
{
    size_t      w = 0;
    number64_be milli;
    int16       tz;

    milli = ntoh_number64(data->date_data.milliseconds);
    w += write_proc(&milli, sizeof(number64_be), user_data);
    tz = ntoh_sint16(data->date_data.timezone);
    w += write_proc(&tz, sizeof(int16), user_data);

    return w;
}

// structure used to mimic a stream with a memory buffer
struct buffer_context {
    byte   *start_address;
    byte   *current_address;
    size_t  buffer_size;
};

// callback function to mimic fread using a memory buffer
static size_t buffer_read(void *out_buffer, size_t size, void *user_data)
{
    buffer_context *ctxt = (buffer_context *)user_data;
    if (ctxt->current_address >= ctxt->start_address &&
        ctxt->current_address + size <= ctxt->start_address + ctxt->buffer_size) {

        memcpy(out_buffer, ctxt->current_address, size);
        ctxt->current_address += size;
        return size;
    }
    else {
        return 0;
    }
}

// callback function to mimic fwrite using a memory buffer
static size_t buffer_write(const void *in_buffer, size_t size, void *user_data)
{
    buffer_context *ctxt = (buffer_context *)user_data;
    if (ctxt->current_address >= ctxt->start_address &&
        ctxt->current_address + size <= ctxt->start_address + ctxt->buffer_size) {

        memcpy(ctxt->current_address, in_buffer, size);
        ctxt->current_address += size;
        return size;
    }
    else {
        return 0;
    }
}

// callback function to read data from a file stream
static size_t file_read(void *out_buffer, size_t size, void *user_data)
{
    return fread(out_buffer, sizeof(byte), size, (FILE *)user_data);
}

// callback function to write data to a file stream
static size_t file_write(const void *in_buffer, size_t size, void *user_data)
{
    return fwrite(in_buffer, sizeof(byte), size, (FILE *)user_data);
}


// function common to all array types
static void amf_list_init(amf_list *list)
{
    if (list != nullptr) {
        list->size          = 0;
        list->first_element = nullptr;
        list->last_element  = nullptr;
    }
}

static amf_data *amf_list_push(amf_list *list, amf_data *data)
{
    amf_node *node = (amf_node *)malloc(sizeof(amf_node));
    if (node != nullptr) {
        node->data = data;
        node->next = nullptr;
        node->prev = nullptr;
        if (list->size == 0) {
            list->first_element = node;
            list->last_element  = node;
        }
        else {
            list->last_element->next = node;
            node->prev = list->last_element;
            list->last_element = node;
        }
        ++(list->size);
        return data;
    }
    return nullptr;
}

static amf_data *amf_list_insert_before(amf_list *list, amf_node *node, amf_data *data)
{
    if (node != nullptr) {
        amf_node *new_node = (amf_node *)malloc(sizeof(amf_node));
        if (new_node != nullptr) {
            new_node->next = node;
            new_node->prev = node->prev;

            if (node->prev != nullptr) {
                node->prev->next = new_node;
                node->prev       = new_node;
            }
            else {
                list->first_element = new_node;
            }
            ++(list->size);
            new_node->data = data;
            return data;
        }
    }
    return nullptr;
}

static amf_data *amf_list_insert_after(amf_list *list, amf_node *node, amf_data *data)
{
    if (node != nullptr) {
        amf_node *new_node = (amf_node *)malloc(sizeof(amf_node));
        if (new_node != nullptr) {
            new_node->next = node->next;
            new_node->prev = node;

            if (node->next != nullptr) {
                node->next->prev = new_node;
                node->next       = new_node;
            }
            else {
                list->last_element = new_node;
            }
            ++(list->size);
            new_node->data = data;
            return data;
        }
    }
    return nullptr;
}

static amf_data *amf_list_delete(amf_list *list, amf_node *node)
{
    amf_data *data = nullptr;
    if (node != nullptr) {
        if (node->next != nullptr) {
            node->next->prev = node->prev;
        }
        if (node->prev != nullptr) {
            node->prev->next = node->next;
        }
        if (node == list->first_element) {
            list->first_element = node->next;
        }
        if (node == list->last_element) {
            list->last_element = node->prev;
        }
        data = node->data;
        free(node);
        --(list->size);
    }
    return data;
}

static amf_data *amf_list_get_at(const amf_list *list, uint32 n)
{
    if (n < list->size) {
        uint32 i;
        amf_node *node = list->first_element;
        for (i = 0; i < n; ++i) {
            node = node->next;
        }
        return node->data;
    }
    return nullptr;
}

static amf_data *amf_list_pop(amf_list *list)
{
    return amf_list_delete(list, list->last_element);
}

static amf_node *amf_list_first(const amf_list *list)
{
    return list->first_element;
}

static amf_node *amf_list_last(const amf_list *list)
{
    return list->last_element;
}

static void amf_list_clear(amf_list *list)
{
    amf_node *tmp;
    amf_node *node = list->first_element;
    while (node != nullptr) {
        amf_data_free(node->data);
        tmp  = node;
        node = node->next;
        free(tmp);
    }
    list->size = 0;
}

static amf_list *amf_list_clone(const amf_list *list, amf_list *out_list)
{
    amf_node *node;
    node = list->first_element;
    while (node != nullptr) {
        amf_list_push(out_list, amf_data_clone(node->data));
        node = node->next;
    }
    return out_list;
}



// load AMF data from stream
amf_data *amf_data_read(amf_read_proc read_proc, void *user_data)
{
    byte type;
    if(read_proc(&type, sizeof(byte), user_data) < sizeof(byte)) {
        return amf_data_error(AMF_ERROR_EOF);
    }

    switch (type) {
    case AMF_TYPE_NUMBER:
        return amf_number_read(read_proc, user_data);
    case AMF_TYPE_BOOLEAN:
        return amf_boolean_read(read_proc, user_data);
    case AMF_TYPE_STRING:
        return amf_string_read(read_proc, user_data);
    case AMF_TYPE_OBJECT:
        return amf_object_read(read_proc, user_data);
    case AMF_TYPE_NULL:
        return amf_null_new();
    case AMF_TYPE_UNDEFINED:
        return amf_undefined_new();
    //case AMF_TYPE_REFERENCE:
    case AMF_TYPE_ECMA_ARRAY:
        return amf_associative_array_read(read_proc, user_data);
    case AMD_TYPE_STRICT_ARRAY:
        return amf_array_read(read_proc, user_data);
    case AMF_TYPE_DATE:
        return amf_date_read(read_proc, user_data);
    //case AMF_TYPE_SIMPLE_OBJ
    case AMF_TYPE_XML:
    case AMF_TYPE_CLASS:
        return amf_data_error(AMF_ERROR_UNSUPPORTED_TYPE);
    case AMF_TYPE_OBJECT_AND_MARKER:
        return amf_data_error(AMF_ERROR_END_TAG); /* end of composite object */
    default:
        return amf_data_error(AMF_ERROR_UNKNOWN_TYPE);
    }
}

size_t amf_data_write(const amf_data *data, amf_write_proc write_proc, void *user_data)
{
    size_t s = 0;
    if (data != nullptr) {
        s += write_proc(&(data->type), sizeof(byte), user_data);
        switch (data->type) {
        case AMF_TYPE_NUMBER:
            s += amf_number_write(data, write_proc, user_data);
            break;
        case AMF_TYPE_BOOLEAN:
            s += amf_boolean_write(data, write_proc, user_data);
            break;
        case AMF_TYPE_STRING:
            s += amf_string_write(data, write_proc, user_data);
            break;
        case AMF_TYPE_OBJECT:
            s += amf_object_write(data, write_proc, user_data);
            break;
        case AMF_TYPE_NULL:
        case AMF_TYPE_UNDEFINED:
            break;
        // case AMF_TYPE_REFERENCE:
        case AMF_TYPE_ECMA_ARRAY:
            s += amf_associative_array_write(data, write_proc, user_data);
            break;
        case AMD_TYPE_STRICT_ARRAY:
            s += amf_array_write(data, write_proc, user_data);
            break;
        case AMF_TYPE_DATE:
            s += amf_date_write(data, write_proc, user_data);
            break;
        /*case AMF_TYPE_SIMPLEOBJECT:*/
        case AMF_TYPE_XML:
        case AMF_TYPE_CLASS:
        case AMF_TYPE_OBJECT_AND_MARKER:
            break; /* end of composite object */
        default:
            break;
        }
    }
    return s;
}


// allocate an AMF data object
amf_data *amf_data_new(byte type)
{
    amf_data *data = (amf_data *)malloc(sizeof(amf_data));
    if (data != nullptr) {
        data->type       = type;
        data->error_code = AMF_ERROR_OK;
    }
    return data;
}

// load AMF data from buffer
amf_data *amf_data_buffer_read(byte *buffer, size_t max_bytes)
{
    buffer_context ctxt;
    ctxt.start_address = ctxt.current_address = buffer;
    ctxt.buffer_size   = max_bytes;
    return amf_data_read(buffer_read, &ctxt);
}

// load AMF data from stream
amf_data *amf_data_file_read(FILE *stream)
{
    return amf_data_read(file_read, stream);
}

// AMF data size
size_t amf_data_size(const amf_data *data)
{
    size_t    s = 0;
    amf_node *node;
    if (data != nullptr) {
        s += sizeof(byte);
        switch (data->type) {
        case AMF_TYPE_NUMBER:
            s += sizeof(number64_be);
            break;
        case AMF_TYPE_BOOLEAN:
            s += sizeof(uint8);
            break;
        case AMF_TYPE_STRING:
            s += sizeof(uint16) + (size_t)amf_string_get_size(data);
            break;
        case AMF_TYPE_OBJECT:
            node = amf_object_first(data);
            while (node != nullptr) {
                s   += sizeof(uint16) + (size_t)amf_string_get_size(amf_object_get_name(node));
                s   += (size_t)amf_data_size(amf_object_get_data(node));
                node = amf_object_next(node);
            }
            s += sizeof(uint16) + sizeof(uint8);
            break;
        case AMF_TYPE_NULL:
        case AMF_TYPE_UNDEFINED:
            break;
        // case AMF_TYPE_REFERENCE:
        case AMF_TYPE_ECMA_ARRAY:
            s   += sizeof(uint32);
            node = amf_associative_array_first(data);
            while (node != nullptr) {
                s   += sizeof(uint16) + (size_t)amf_string_get_size(amf_associative_array_get_name(node));
                s   += (size_t)amf_data_size(amf_associative_array_get_data(node));
                node = amf_associative_array_next(node);
            }
            s += sizeof(uint16) + sizeof(uint8);
            break;
        case AMD_TYPE_STRICT_ARRAY:
            s   += sizeof(uint32);
            node = amf_array_first(data);
            while (node != nullptr) {
                s   += (size_t)amf_data_size(amf_array_get(node));
                node = amf_array_next(node);
            }
            break;
        case AMF_TYPE_DATE:
            s += sizeof(number64) + sizeof(uint16);
            break;
        /*case AMF_TYPE_SIMPLEOBJECT:*/
        case AMF_TYPE_XML:
        case AMF_TYPE_CLASS:
        case AMF_TYPE_OBJECT_AND_MARKER:
            break; /* end of composite object */
        default:
            break;
        }
    }
    return s;
}

// write encoded AMF data into a buffer
size_t amf_data_buffer_write(amf_data *data, byte *buffer, size_t max_bytes)
{
    buffer_context ctxt{};
    ctxt.start_address = ctxt.current_address = buffer;
    ctxt.buffer_size   = max_bytes;
    return amf_data_write(data, buffer_write, &ctxt);
}

// write encoded AMF data into a stream
size_t amf_data_file_write(const amf_data *data, FILE *stream)
{
    return amf_data_write(data, file_write, stream);
}

// get the type of AMF data
byte amf_data_get_type(const amf_data *data)
{
    return (data != nullptr) ? data->type : AMF_TYPE_NULL;
}

// get the error code of AMF data
byte amf_data_get_error_code(const amf_data *data)
{
    return (data != nullptr) ? data->error_code : AMF_ERROR_NULL_POINTER;
}

// return a new copy of AMF data
amf_data *amf_data_clone(const amf_data *data)
{
    // we copy data recursively
    if (data != nullptr) {
        switch (data->type) {
        case AMF_TYPE_NUMBER:
            return amf_number_new(amf_number_get_value(data));
        case AMF_TYPE_BOOLEAN:
            return amf_boolean_new(amf_boolean_get_value(data));
        case AMF_TYPE_STRING:
            if (data->string_data.mbstr != nullptr) {
                return amf_string_new((byte *)strdup((char *)amf_string_get_bytes(data)), amf_string_get_size(data));
            }
            else {
                return amf_str(nullptr);
            }
        case AMF_TYPE_NULL:
            return nullptr;
        case AMF_TYPE_UNDEFINED:
            return nullptr;
        // case AMF_TYPE_REFERENCE:*/
        case AMF_TYPE_OBJECT:
        case AMF_TYPE_ECMA_ARRAY:
        case AMD_TYPE_STRICT_ARRAY: {
            amf_data *d = amf_data_new(data->type);
            if (d != nullptr) {
                amf_list_init (&d->list_data);
                amf_list_clone(&data->list_data, &d->list_data);
            }
            return d;
        }
        case AMF_TYPE_DATE:
            return amf_date_new(amf_date_get_milliseconds(data), amf_date_get_timezone(data));
        //case AMF_TYPE_SIMPLEOBJECT:
        case AMF_TYPE_XML:
            return nullptr;
        case AMF_TYPE_CLASS:
            return nullptr;
        }
    }
    return nullptr;
}

//  release the memory of AMF data
void amf_data_free(amf_data *data)
{
    if (data != nullptr) {
        switch (data->type) {
        case AMF_TYPE_NUMBER:
            break;
        case AMF_TYPE_BOOLEAN:
            break;
        case AMF_TYPE_STRING:
            if (data->string_data.mbstr != nullptr) {
                free(data->string_data.mbstr);
            }
            break;
        case AMF_TYPE_NULL:
            break;
        case AMF_TYPE_UNDEFINED:
            break;
        /*case AMF_TYPE_REFERENCE:*/
        case AMF_TYPE_OBJECT:
        case AMF_TYPE_ECMA_ARRAY:
        case AMD_TYPE_STRICT_ARRAY:
            amf_list_clear(&data->list_data);
            break;
        case AMF_TYPE_DATE:
            break;
        /*case AMF_TYPE_SIMPLEOBJECT:*/
        case AMF_TYPE_XML:
            break;
        case AMF_TYPE_CLASS:
            break;
        default:
            break;
        }
        free(data);
    }
}

// dump AMF data into a stream as text
void amf_data_dump(FILE *stream, const amf_data *data, int indent_level)
{
    if (data != nullptr) {
        amf_node *node;
        char      datestr[128];
        switch (data->type) {
        case AMF_TYPE_NUMBER:
            fprintf(stream, "%.12g", data->number_data);
            break;
        case AMF_TYPE_BOOLEAN:
            fprintf(stream, "%s", (data->boolean_data) ? "true" : "false");
            break;
        case AMF_TYPE_STRING:
            fprintf(stream, "\'%.*s\'", data->string_data.size, data->string_data.mbstr);
            break;
        case AMF_TYPE_OBJECT:
            node = amf_object_first(data);
            fprintf(stream, "{\n");
            while (node != nullptr) {
                fprintf(stream, "%*s", (indent_level + 1) * 4, "");
                amf_data_dump(stream, amf_object_get_name(node), indent_level + 1);
                fprintf(stream, ": ");
                amf_data_dump(stream, amf_object_get_data(node), indent_level + 1);
                node = amf_object_next(node);
                fprintf(stream, "\n");
            }
            fprintf(stream, "%*s", indent_level * 4 + 1, "}");
            break;
        case AMF_TYPE_NULL:
            fprintf(stream, "null");
            break;
        case AMF_TYPE_UNDEFINED:
            fprintf(stream, "undefined");
            break;
        /*case AMF_TYPE_REFERENCE:*/
        case AMF_TYPE_ECMA_ARRAY:
            node = amf_associative_array_first(data);
            fprintf(stream, "{\n");
            while (node != nullptr) {
                fprintf(stream, "%*s", (indent_level + 1) * 4, "");
                amf_data_dump(stream, amf_associative_array_get_name(node), indent_level + 1);
                fprintf(stream, " => ");
                amf_data_dump(stream, amf_associative_array_get_data(node), indent_level + 1);
                node = amf_associative_array_next(node);
                fprintf(stream, "\n");
            }
            fprintf(stream, "%*s", indent_level * 4 + 1, "}");
            break;
        case AMD_TYPE_STRICT_ARRAY:
            node = amf_array_first(data);
            fprintf(stream, "[\n");
            while (node != nullptr) {
                fprintf(stream, "%*s", (indent_level + 1) * 4, "");
                amf_data_dump(stream, node->data, indent_level + 1);
                node = amf_array_next(node);
                fprintf(stream, "\n");
            }
            fprintf(stream, "%*s", indent_level * 4 + 1, "]");
            break;
        case AMF_TYPE_DATE:
            amf_date_to_iso8601(data, datestr, sizeof(datestr));
            fprintf(stream, "%s", datestr);
            break;
        /*case AMF_TYPE_SIMPLEOBJECT:*/
        case AMF_TYPE_XML:
            break;
        case AMF_TYPE_CLASS:
            break;
        default:
            break;
        }
    }
}

// return a null AMF object with the specified error code attached to it
amf_data *amf_data_error(byte error_code)
{
    amf_data *data = amf_null_new();
    if (data != nullptr) {
        data->error_code = error_code;
    }
    return data;
}

// number functions
amf_data *amf_number_new(number64 value)
{
    amf_data *data = amf_data_new(AMF_TYPE_NUMBER);
    if (data != nullptr) {
        data->number_data = value;
    }
    return data;
}

number64 amf_number_get_value(const amf_data *data)
{
    return (data != nullptr) ? data->number_data : 0;
}
void amf_number_set_value(amf_data *data, number64 value)
{
    if (data != nullptr) {
        data->number_data = value;
    }
}

// boolean functions
amf_data *amf_boolean_new(uint8 value)
{
    amf_data *data = amf_data_new(AMF_TYPE_BOOLEAN);
    if (data != nullptr) {
        data->boolean_data = value;
    }
    return data;
}

uint8 amf_boolean_get_value(const amf_data *data)
{
    return (data != nullptr) ? data->boolean_data : 0;
}

void amf_boolean_set_value(amf_data *data, uint8 value)
{
    if (data != nullptr) {
        data->boolean_data = value;
    }
}

// string functions
amf_data *amf_string_new(byte *str, uint16 size)
{
    amf_data *data = amf_data_new(AMF_TYPE_STRING);
    if (data != nullptr) {
        if (str == nullptr) data->string_data.size = 0;
        else                data->string_data.size = size;

        data->string_data.mbstr = (byte *)calloc(size + 1, sizeof(byte));
        if (data->string_data.mbstr != nullptr) {
            if (data->string_data.size > 0) {
                memcpy(data->string_data.mbstr, str, data->string_data.size);
            }
        }
        else {
            amf_data_free(data);
            return nullptr;
        }
    }
    return data;
}

amf_data *amf_str(const char *str)
{
    return amf_string_new((byte *)str, (uint16)(str != nullptr ? strlen(str) : 0));
}

uint16 amf_string_get_size (const amf_data *data)
{
    return (data != nullptr) ? data->string_data.size : 0;
}

byte *amf_string_get_bytes(const amf_data *data)
{
    return (data != nullptr) ? data->string_data.mbstr : nullptr;
}

// object functions
amf_data *amf_object_new(void)
{
    amf_data *data = amf_data_new(AMF_TYPE_OBJECT);
    if (data != nullptr)
        amf_list_init(&data->list_data);

    return data;
}

uint32 amf_object_size(const amf_data *data)
{
    return (data != nullptr) ? data->list_data.size / 2 : 0;
}


amf_data *amf_object_add(amf_data *data, const char *name, amf_data *element)
{
    if (data != nullptr) {
        if (amf_list_push(&data->list_data, amf_str(name)) != nullptr) {
            if (amf_list_push(&data->list_data, element) != nullptr)
                return element;
            else
                amf_data_free(amf_list_pop(&data->list_data));
        }
    }
    return nullptr;
}

amf_data *amf_object_get(const amf_data *data, const char *name)
{
    if (data != nullptr) {
        amf_node *node = amf_list_first(&(data->list_data));
        while (node != nullptr) {
            if (strncmp((char *)(node->data->string_data.mbstr), name, (size_t)(node->data->string_data.size)) == 0) {
                node = node->next;
                return (node != nullptr) ? node->data : nullptr;
            }
            // we have to skip the element data to reach the next name
            node = node->next->next;
        }
    }
    return nullptr;
}

amf_data *amf_object_set(amf_data *data, const char *name, amf_data *element)
{
    if (data != nullptr) {
        amf_node *node = amf_list_first(&(data->list_data));
        while (node != nullptr) {
            if (strncmp((char *)(node->data->string_data.mbstr), name, (size_t)(node->data->string_data.size)) == 0) {
                node = node->next;
                if (node != nullptr && node->data != nullptr) {
                    amf_data_free(node->data);
                    node->data = element;
                    return element;
                }
            }
            // we have to skip the element data to reach the next name
            node = node->next->next;
        }
    }
    return nullptr;
}

amf_data *amf_object_delete(amf_data *data, const char *name)
{
    if (data != nullptr) {
        amf_node *node = amf_list_first(&data->list_data);
        while (node != nullptr) {
            node = node->next;
            if (strncmp((char *)(node->data->string_data.mbstr), name, (size_t)(node->data->string_data.size)) == 0) {
                amf_node *data_node = node->next;
                amf_data_free(amf_list_delete(&data->list_data, node));
                return amf_list_delete(&data->list_data, data_node);
            }
            else {
                node = node->next;
            }
        }
    }
    return nullptr;
}

amf_node *amf_object_first(const amf_data *data)
{
    return (data != nullptr) ? amf_list_first(&data->list_data) : nullptr;
}

amf_node *amf_object_last(const amf_data *data)
{
    if (data != nullptr) {
        amf_node *node = amf_list_last(&data->list_data);
        if (node != nullptr)
            return node->prev;
    }
    return nullptr;
}

amf_node *amf_object_next(amf_node *node)
{
    if (node != nullptr) {
        amf_node *next = node->next;
        if (next != nullptr)
            return next->next;
    }
    return nullptr;
}

amf_node *amf_object_prev(amf_node *node)
{
    if (node != nullptr) {
        amf_node *prev = node->prev;
        if (prev != nullptr)
            return prev->prev;
    }
    return nullptr;
}

amf_data *amf_object_get_name(amf_node *node)
{
    return (node != nullptr) ? node->data : nullptr;
}

amf_data *amf_object_get_data(amf_node *node)
{
    if (node != nullptr) {
        amf_node *next = node->next;
        if (next != nullptr)
            return next->data;
    }
    return nullptr;
}


amf_data *amf_associative_array_new(void)
{
    amf_data *data = amf_data_new(AMF_TYPE_ECMA_ARRAY);
    if (data != nullptr) {
        amf_list_init(&data->list_data);
    }
    return data;
}

amf_data *amf_array_new(void)
{
    amf_data *data = amf_data_new(AMD_TYPE_STRICT_ARRAY);
    if (data != nullptr) {
        amf_list_init(&data->list_data);
    }
    return data;
}

uint32    amf_array_size(const amf_data *data)
{
    return (data != nullptr) ? data->list_data.size : 0;
}

amf_data *amf_array_push(amf_data *data, amf_data *element)
{
    return (data != nullptr) ? amf_list_push(&data->list_data, element) : nullptr;
}

amf_data *amf_array_pop(amf_data *data)
{
    return (data != nullptr) ? amf_list_pop(&data->list_data) : nullptr;
}

amf_node *amf_array_first(const amf_data *data)
{
    return (data != nullptr) ? amf_list_first(&data->list_data) : nullptr;
}

amf_node *amf_array_last(const amf_data *data)
{
    return (data != nullptr) ? amf_list_last(&data->list_data) : nullptr;
}

amf_node *amf_array_next(amf_node *node)
{
    return (node != nullptr) ? node->next : nullptr;
}

amf_node *amf_array_prev(amf_node *node)
{
    return (node != nullptr) ? node->prev : nullptr;
}

amf_data *amf_array_get(amf_node *node)
{
    return (node != nullptr) ? node->data : nullptr;
}

amf_data *amf_array_get_at(const amf_data *data, uint32 n)
{
    return (data != nullptr) ? amf_list_get_at(&data->list_data, n) : nullptr;
}

amf_data *amf_array_delete(amf_data *data, amf_node *node)
{
    return (data != nullptr) ? amf_list_delete(&data->list_data, node) : nullptr;
}

amf_data *amf_array_insert_before(amf_data *data, amf_node *node, amf_data *element)
{
    return (data != nullptr) ? amf_list_insert_before(&data->list_data, node, element) : nullptr;
}

amf_data *amf_array_insert_after(amf_data *data, amf_node *node, amf_data *element)
{
    return (data != nullptr) ? amf_list_insert_after(&data->list_data, node, element) : nullptr;
}

// date functions
amf_data *amf_date_new(number64 milliseconds, uint16 timezone)
{
    amf_data *data = amf_data_new(AMF_TYPE_DATE);
    if (data != nullptr) {
        data->date_data.milliseconds = milliseconds;
        data->date_data.timezone = timezone;
    }
    return data;
}

number64  amf_date_get_milliseconds(const amf_data *data)
{
    return (data != nullptr) ? data->date_data.milliseconds : 0.0;
}

uint16 amf_date_get_timezone(const amf_data *data)
{
    return (data != nullptr) ? data->date_data.timezone : 0;
}

time_t amf_date_to_time_t(const amf_data *data)
{
    return (time_t)((data != nullptr) ? data->date_data.milliseconds / 1000 : 0);
}

size_t amf_date_to_iso8601(const amf_data *data, char *buffer, size_t buf_size)
{
    struct tm *t;
    time_t     time = amf_date_to_time_t(data);

    tzset();
    t = localtime(&time);
    if (t != nullptr) {
        return strftime(buffer, buf_size, "%Y-%m-%dT%H:%M:%S", t);
    }
    else {
        /* if we couldn't parse the date, use a default value */
        return snprintf(buffer, buf_size, "0000-00-00T00:00:00");
    }
}



#ifdef __cplusplus
};
#endif


NS_END
