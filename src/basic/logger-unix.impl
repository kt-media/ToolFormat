#include "basic/ILogger.hpp"
#include "basic/Types.hpp"

#include <cstdarg>
#include <cstdio>
#include <cstdlib>
#include <sstream>
#include <iostream>
#include <string>

NS_BEGIN

const uint32 MaxLogBufferLength = 2048;

static void printLog(LogLevel level, const char *tag, const char *file, const char* func, int line, const char *message)
{
    std::stringstream ss;
    switch (level)
    {
        case DEBUG:
            {
                ss << "[D/";
            }
            break;
        case INFO:
            {
                ss << "[I/";
            }
            break;
        case WARN:
            {
                ss << "[W/";
            }
            break;
        case ERROR:
            {
                ss << "[E/";
            }
            break;
    }

    ss << file << ":" << line << "@" << func << "/" << tag << "] " << message;

    std::cout << ss.str() << std::endl;
}


class UnixLogger: public ILogger
{
public:
    UnixLogger() = default;
    ~UnixLogger() override = default;

public:
    void debug(const char *tag, const char *file, const char* func, int line, const char *format, ...) override
    {
        char buffer[MaxLogBufferLength] = {0x00};
        va_list ap;
        va_start(ap, format);
        int n = vsnprintf(buffer, MaxLogBufferLength, format, ap);
        va_end(ap);
        printLog(DEBUG, tag, file, func, line, buffer);
    }

    void info (const char *tag, const char *file, const char* func, int line, const char *format, ...) override
    {
        char buffer[MaxLogBufferLength] = {0x00};
        va_list ap;
        va_start(ap, format);
        int n = vsnprintf(buffer, MaxLogBufferLength, format, ap);
        va_end(ap);
        printLog(INFO, tag, file, func, line, buffer);
    }

    void warn (const char *tag, const char *file, const char* func, int line, const char *format, ...) override
    {
        char buffer[MaxLogBufferLength] = {0x00};
        va_list ap;
        va_start(ap, format);
        int n = vsnprintf(buffer, MaxLogBufferLength, format, ap);
        va_end(ap);
        printLog(WARN, tag, file, func, line, buffer);
    }

    void error(const char *tag, const char *file, const char* func, int line, const char *format, ...) override
    {
        char buffer[MaxLogBufferLength] = {0x00};
        va_list ap;
        va_start(ap, format);
        int n = vsnprintf(buffer, MaxLogBufferLength, format, ap);
        va_end(ap);
        printLog(ERROR, tag, file, func, line, buffer);
    }
};


NS_END
