#include "basic/ILogger.hpp"
#include "basic/Platform.hpp"


#if OS_TYPE == OS_ANDROID
#   include "logger-android.impl"
#else
#   include "logger-unix.impl"
#endif

NS_BEGIN


ILogger& ILogger::instance()
{
#if OS_TYPE == OS_ANDROID
    // TODO
#else
    static UnixLogger logger;
    return logger;
#endif
}


NS_END
