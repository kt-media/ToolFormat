#include "basic/Types.hpp"

NS_BEGIN

#ifdef __cplusplus
extern "C" {
#endif

#if !defined(KT_BIG_ENDIAN)
typedef union _convert_u {
    uint64   i;
    number64 f;
} convert_u;
#endif


uint16 ntoh_uint16(uint16 x)
{
#if defined(KT_BIG_ENDIAN)
    return x;
#else
    uint32 value = (x & 0x00FFU) << 8;
    value       |= (x & 0xFF00U) >> 8;
    return value;
#endif
}

int16 ntoh_sint16(int16 x)
{
#if defined(KT_BIG_ENDIAN)
    return x;
#else
    int16 value = (x & 0x00FF) << 8;
    value      |= (x & 0xFF00) >> 8;
    return value;
#endif
}

uint32 ntoh_uint32(uint32 x)
{
#if defined(KT_BIG_ENDIAN)
    return x;
#else
    uint32 value = (x & 0x000000FFU) << 24;
    value       |= (x & 0x0000FF00U) << 8;
    value       |= (x & 0x00FF0000U) >> 8;
    value       |= (x & 0xFF000000U) >> 24;
    return value;
#endif
}

uint32 ntoh_uint24(uint24 x)
{
    uint32 value = x.b[0] << 16;
    value       |= x.b[1] << 8;
    value       |= x.b[2];
    return value;
}

int32 ntoh_uint24_3(u8 x, u8 y, u8 z)
{
    int32 value = 0;
    value      |= x << 16 ;
    value      |= y << 8 ;
    value      |= z;
    return value;
}


number64 ntoh_number64(number64 x)
{
#if defined(KT_BIG_ENDIAN)
    return x;
#else
    convert_u c;
    uint64    v = x;
    c.i  = 0;
    c.i |= (v & 0x00000000000000FFULL) << 56;
    c.i |= (v & 0x000000000000FF00ULL) << 40;
    c.i |= (v & 0x0000000000FF0000ULL) << 24;
    c.i |= (v & 0x00000000FF000000ULL) << 8;
    c.i |= (v & 0x000000FF00000000ULL) >> 8;
    c.i |= (v & 0x0000FF0000000000ULL) >> 24;
    c.i |= (v & 0x00FF000000000000ULL) >> 40;
    c.i |= (v & 0xFF00000000000000ULL) >> 56;
    return c.f;
#endif
}

number64 hton_number64(number64 x)
{
#if defined(KT_BIG_ENDIAN)
    return x;
#else
    convert_u c;
    uint64    v = x;
    c.i  = 0;
    c.i |= (v & 0xFF00000000000000ULL) >> 56;
    c.i |= (v & 0x00FF000000000000ULL) >> 40;
    c.i |= (v & 0x0000FF0000000000ULL) >> 24;
    c.i |= (v & 0x000000FF00000000ULL) >> 8;
    c.i |= (v & 0x00000000FF000000ULL) << 8;
    c.i |= (v & 0x0000000000FF0000ULL) << 24;
    c.i |= (v & 0x000000000000FF00ULL) << 40;
    c.i |= (v & 0x00000000000000FFULL) << 56;
    return c.f;
#endif
}



// convert native integers into 24 bits big endian integers
uint24_be hton_uint32(uint32 l)
{
    uint24_be r;
    r.b[0] = (uint8) ( (l & 0x00FF0000U) >> 16);
    r.b[1] = (uint8) ( (l & 0x0000FF00U) >> 8 );
    r.b[2] = (uint8) (  l & 0x000000FFU);
    return r;
}

#ifdef __cplusplus
}
#endif

NS_END
