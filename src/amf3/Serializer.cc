#include "amf3/Serializer.hpp"
#include "amf3/types/AmfItem.hpp"

namespace amf {

Serializer& Serializer::operator<<(const AmfItem& item)
{
    std::vector<u8> serialized = item.serialize(ctx);
    buf.insert(buf.end(), serialized.begin(), serialized.end());
    return *this;
}

}
