#pragma once

#ifndef _INCLUDE_MP4_DUMP_H
#define _INCLUDE_MP4_DUMP_H

#include "basic/Types.hpp"
#include "basic/Basic.hpp"

NS_BEGIN

int mp4_dump_main(int argc, char *argv[]);

NS_END

#endif//_INCLUDE_MP4_DUMP_H
