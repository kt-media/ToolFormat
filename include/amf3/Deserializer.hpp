#pragma once

#ifndef INCLUDE_DESERIALIZER_H
#define INCLUDE_DESERIALIZER_H

#include <functional>
#include <map>
#include <string>

#include "amf3/utils/AmfItemPtr.hpp"
#include "Amf.hpp"
#include "amf3/types/AmfArray.hpp"
#include "amf3/types/AmfBool.hpp"
#include "amf3/types/AmfDate.hpp"
#include "amf3/types/AmfDictionary.hpp"
#include "amf3/types/AmfDouble.hpp"
#include "amf3/types/AmfInteger.hpp"
#include "amf3/types/AmfNull.hpp"
#include "amf3/types/AmfUndefined.hpp"
#include "amf3/types/AmfVector.hpp"
#include "amf3/types/AmfXml.hpp"
#include "amf3/types/AmfObject.hpp"
#include "amf3/types/AmfString.hpp"
#include "amf3/types/AmfXmlDocument.hpp"
#include "SerializationContext.hpp"

namespace amf {

class AmfObject;

typedef std::function<AmfObject(v8::const_iterator&, v8::const_iterator, SerializationContext&)> ExternalDeserializerFunction;

class Deserializer {
public:
    Deserializer() : ctx() { }
    Deserializer(const SerializationContext& ctx) : ctx(ctx) { }

    AmfItemPtr deserialize(const v8& buf);
    AmfItemPtr deserialize(v8::const_iterator& it, v8::const_iterator end)
    {
        return deserialize(it, end, ctx);
    }

    void clearContext()
    {
        ctx.clear();
    }

    static AmfItemPtr deserialize(const v8& data, SerializationContext& ctx);
    static AmfItemPtr deserialize(v8::const_iterator& it, v8::const_iterator end, SerializationContext& ctx);

    static std::map<std::string, ExternalDeserializerFunction> externalDeserializers;

private:
    SerializationContext ctx;
};

}

#endif//INCLUDE_DESERIALIZER_H
