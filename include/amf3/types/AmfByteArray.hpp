#pragma once

#ifndef INCLUDE_AMF_BYTE_ARRAY_H
#define INCLUDE_AMF_BYTE_ARRAY_H

#include "amf3/types/AmfItem.hpp"


namespace amf {

class SerializationContext;

class AmfByteArray: public AmfItem {
public:
    AmfByteArray() = default;
    AmfByteArray(const AmfByteArray& other) : value(other.value)
    {
    }

    template<typename T>
    explicit AmfByteArray(const T& v)
    {
        using std::begin;
        using std::end;
        value = std::vector<u8>(begin(v), end(v));
    }

    template<typename T>
    AmfByteArray(T begin, T end)
    {
        value = std::vector<u8>(begin, end);
    }

    bool operator==(const AmfItem& other) const override;
    std::vector<u8> serialize(SerializationContext& ctx) const override;
    static AmfByteArray deserialize(v8::const_iterator& it, v8::const_iterator end, SerializationContext& ctx);

    std::vector<u8> value;
};

}


#endif//INCLUDE_AMF_BYTE_ARRAY_H
