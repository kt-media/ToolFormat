#pragma once

#ifndef INCLUDE_AMF_INTEGER_H
#define INCLUDE_AMF_INTEGER_H

#include "AmfItem.hpp"

namespace amf {

class SerializationContext;

class AmfInteger: public AmfItem {
public:
    AmfInteger() : value(0)
    {
    }

    explicit AmfInteger(int v) : value(v)
    {
    }

    operator int() const
    {
        return value;
    }

    bool operator==(const AmfItem& other) const override;
    std::vector<u8> serialize(SerializationContext&) const override;
    static std::vector<u8> asLength(size_t value, u8 marker);
    static AmfInteger deserialize(v8::const_iterator& it, v8::const_iterator end, SerializationContext&);
    static int deserializeValue(v8::const_iterator& it, v8::const_iterator end);

    int value;
};

}


#endif//INCLUDE_AMF_INTEGER_H
