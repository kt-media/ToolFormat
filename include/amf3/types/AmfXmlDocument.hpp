#pragma once

#ifndef INCLUDE_AMF_XML_DOCUMENT_H
#define INCLUDE_AMF_XML_DOCUMENT_H

#include "AmfItem.hpp"

#include <string>
#include <utility>

namespace amf {

class SerializationContext;

class AmfXmlDocument : public AmfItem {
public:
    AmfXmlDocument() = default;
    explicit AmfXmlDocument(std::string value) : value(std::move(value)) { }

    bool operator==(const AmfItem& other) const override;
    std::vector<u8> serialize(SerializationContext& ctx) const override;
    static AmfXmlDocument deserialize(v8::const_iterator& it, v8::const_iterator end, SerializationContext& ctx);

    std::string value;
};

}


#endif//INCLUDE_AMF_XML_DOCUMENT_H

