#pragma once

#ifndef INCLUDE_AMF_NULL_H
#define INCLUDE_AMF_NULL_H

#include <vector>

#include "AmfItem.hpp"

namespace amf {

class SerializationContext;

class AmfNull : public AmfItem {
public:
    AmfNull() = default;

    bool operator==(const AmfItem& other) const override
    {
        const auto *p = dynamic_cast<const AmfNull *>(&other);
        return p != nullptr;
    }

    std::vector<u8> serialize(SerializationContext&) const override
    {
        return std::vector<u8> { AMF_NULL };
    }

    static AmfNull deserialize(v8::const_iterator& it, v8::const_iterator end, SerializationContext&)
    {
        if (it == end || *it++ != AMF_NULL)
            throw std::invalid_argument("AmfNull: Invalid type marker");

        return AmfNull();
    }
};

}



#endif//INCLUDE_AMF_NULL_H
