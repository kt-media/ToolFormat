#pragma once

#ifndef INCLUDE_AMF_STRING_H
#define INCLUDE_AMF_STRING_H

#include <string>
#include <utility>

#include "AmfItem.hpp"


namespace amf {

class SerializationContext;

class AmfString : public AmfItem {
public:
    AmfString() = default;

    explicit AmfString(const char *v) : value(v == nullptr ? "" : v)
    {
    }

    explicit AmfString(std::string v) : value(v)
    {
    }

    explicit operator std::string() const
    {
        return value;
    }

    bool operator==(const AmfItem& other) const override;
    std::vector<u8> serialize(SerializationContext& ctx) const override;
    std::vector<u8> serializeValue(SerializationContext& ctx) const;
    static AmfString deserialize(v8::const_iterator& it, v8::const_iterator end, SerializationContext& ctx);
    static std::string deserializeValue(v8::const_iterator& it, v8::const_iterator end, SerializationContext& ctx);

    std::string value;
};

}



#endif//INCLUDE_AMF_STRING_H
