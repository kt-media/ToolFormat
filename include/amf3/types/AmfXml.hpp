#pragma once

#ifndef INCLUDE_AMF_XML_H
#define INCLUDE_AMF_XML_H

#include <string>

#include "amf3/types/AmfItem.hpp"

namespace amf {

class SerializationContext;

class AmfXml: public AmfItem {
public:
    AmfXml() = default;
    AmfXml(std::string value) : value(value)
    {
    }

    bool operator==(const AmfItem& other) const override;
    std::vector<u8> serialize(SerializationContext& ctx) const override;
    static AmfXml deserialize(v8::const_iterator& it, v8::const_iterator end, SerializationContext& ctx);

    std::string value;
};

}

#endif//INCLUDE_AMF_XML_H
