#pragma once

#ifndef INCLUDE_AMF_DATE_H
#define INCLUDE_AMF_DATE_H


#include <chrono>
#include <ctime>

#include "AmfItem.hpp"


namespace amf {

class SerializationContext;

// this needs to be a long long to ensure no overflow
namespace {
static const long long MSEC_PER_SEC = 1000ll;
}

class AmfDate : public AmfItem {
public:
    // millisconds since epoch
    explicit AmfDate(long long date) : value(date) { }
    explicit AmfDate(std::tm *date) : value(mktime(date) * MSEC_PER_SEC) { }
    explicit AmfDate(std::chrono::system_clock::time_point date);

    bool operator==(const AmfItem& other) const override;
    std::vector<u8> serialize(SerializationContext& ctx) const override;
    static AmfDate deserialize(v8::const_iterator& it, v8::const_iterator end, SerializationContext& ctx);

    long long value;
};

}


#endif//INCLUDE_AMF_DATE_H
