#pragma once

#ifndef INCLUDE_AMF_DOUBLE_H
#define INCLUDE_AMF_DOUBLE_H

#include "AmfItem.hpp"

namespace amf {

class SerializationContext;

class AmfDouble : public AmfItem {
public:
    AmfDouble() : value(0) { }

    explicit AmfDouble(double v) : value(v) { }

    explicit operator double() const
    {
        return value;
    }

    bool operator==(const AmfItem& other) const override;
    std::vector<u8> serialize(SerializationContext&) const override;
    static AmfDouble deserialize(v8::const_iterator& it, v8::const_iterator end, SerializationContext&);

    double value;
};

}

#endif//INCLUDE_AMF_DOUBLE_H
