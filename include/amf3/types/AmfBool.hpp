#pragma once

#ifndef AMF_BOOLEAN_H
#define AMF_BOOLEAN_H

#include "AmfItem.hpp"

namespace amf
{

class SerializationContext;

class AmfBool: public AmfItem
{
public:
    explicit AmfBool(bool v): value(v)
    {
    }

    explicit operator bool() const
    {
        return value;
    }

    bool operator==(const AmfItem &other) const override;

    std::vector<u8> serialize(SerializationContext &) const override
    {
        return std::vector<u8>{value ? AMF_TRUE : AMF_FALSE};
    }

    static AmfBool deserialize(v8::const_iterator &it, v8::const_iterator end, SerializationContext &);

    bool value;
};

}

#endif//AMF_BOOLEAN_H
