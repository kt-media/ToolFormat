#pragma once

#ifndef INCLUDE_AMF_UNDEFINED_H
#define INCLUDE_AMF_UNDEFINED_H

#include <vector>

#include "AmfItem.hpp"

namespace amf {

class SerializationContext;

class AmfUndefined : public AmfItem {
public:
    AmfUndefined() = default;

    bool operator==(const AmfItem& other) const override
    {
        const auto *p = dynamic_cast<const AmfUndefined *>(&other);
        return p != nullptr;
    }

    std::vector<u8> serialize(SerializationContext&) const override
    {
        return std::vector<u8> { AMF_UNDEFINED };
    }

    static AmfUndefined deserialize(v8::const_iterator& it, v8::const_iterator end, SerializationContext&)
    {
        if (it == end || *it++ != AMF_UNDEFINED)
            throw std::invalid_argument("AmfUndefined: Invalid type marker");
        return AmfUndefined();
    }

};

}

#endif//INCLUDE_AMF_UNDEFINED_H
