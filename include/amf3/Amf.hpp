//
// A C++ implementation of the Action Message Format (AMF3)
// https://github.com/Ventero/amf-cpp
//
// https://www.jianshu.com/p/018f4b13feaf?utm_campaign=maleskine&utm_content=note&utm_medium=seo_notes&utm_source=recommendation
//
// rtmpdump
// http://rtmpdump.mplayerhq.hu/
// some document
// https://github.com/jldavid/lf-objc/blob/master/README.md
// AMF3: https://www.adobe.com/content/dam/acom/en/devnet/pdf/amf-file-format-spec.pdf
// RTMP: https://wwwimages2.adobe.com/content/dam/acom/en/devnet/rtmp/pdf/rtmp_specification_1.0.pdf
//


#pragma once

#ifndef INCLUDE_AMF_H
#define INCLUDE_AMF_H

#include <algorithm>
#include <cstdint>
#include <stdexcept>
#include <vector>

// Windows only runs on little endian platforms
#ifdef _WIN32
#   define LITTLE_ENDIAN    1234
#   define BIG_ENDIAN       4321
#   define BYTE_ORDER       LITTLE_ENDIAN
#elif defined(__APPLE__)
#   include <machine/endian.h>
#else
#   include <endian.h>
#endif


namespace amf {

typedef unsigned char   u8;
typedef std::vector<u8> v8;

template <typename T>
T swap_endian(T x)
{
    u8 *bytes = reinterpret_cast<u8 *>(&x);
    std::reverse(bytes, bytes + sizeof(T));
    return x;
}

#if BYTE_ORDER == LITTLE_ENDIAN
#   define hton(x) swap_endian(x)
#   define ntoh(x) swap_endian(x)
#elif BYTE_ORDER == BIG_ENDIAN
#   define hton(x) (x)
#   define ntoh(x) (x)
#else
#   error Endianness not supported
#endif

template <typename T>
v8 network_bytes(T x)
{
    T swapped       = hton(x);
    const u8 *bytes = reinterpret_cast<const u8 *>(&swapped);
    return v8(bytes, bytes + sizeof(T));
}

template<typename T>
T read_network(v8::const_iterator& it, v8::const_iterator end)
{
    if (static_cast<size_t>(end - it) < sizeof(T))
        throw std::out_of_range("Not enough bytes to read");

    T val;
    std::copy(it, it + sizeof(T), reinterpret_cast<u8 *>(&val));
    it += sizeof(T);

    return ntoh(val);
}

}

#endif//INCLUDE_AMF_H
