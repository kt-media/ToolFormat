#pragma once

#ifndef INCLUDE_SERIALIZER_H
#define INCLUDE_SERIALIZER_H

#include <vector>

#include "Amf.hpp"
#include "amf3/types/AmfArray.hpp"
#include "amf3/types/AmfBool.hpp"
#include "amf3/types/AmfDate.hpp"
#include "amf3/types/AmfDictionary.hpp"
#include "amf3/types/AmfDouble.hpp"
#include "amf3/types/AmfInteger.hpp"
#include "amf3/types/AmfNull.hpp"
#include "amf3/types/AmfUndefined.hpp"
#include "amf3/types/AmfVector.hpp"
#include "amf3/types/AmfXml.hpp"
#include "amf3/types/AmfObject.hpp"
#include "amf3/types/AmfString.hpp"
#include "amf3/types/AmfXmlDocument.hpp"
#include "SerializationContext.hpp"

namespace amf {

class AmfItem;

class Serializer {
public:
    Serializer() = default;

    ~Serializer() = default;

    Serializer& operator<<(const AmfItem& item);

    [[nodiscard]] const std::vector<u8>& data() const
    {
        return buf;
    }

    void clear()
    {
        buf.clear();
        ctx.clear();
    }

private:
    SerializationContext ctx;
    std::vector<u8>      buf;
};

}

#endif//INCLUDE_SERIALIZER_H
