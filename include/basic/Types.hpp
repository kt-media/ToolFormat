#ifndef INCLUDE_TYPES_H
#define INCLUDE_TYPES_H

#include "Basic.hpp"
#include <vector>

// Configuration of the sources
#ifdef HAVE_CONFIG_H
#   include <config.h>
#endif

#ifdef HAVE_STDINT_H
#   include <cstdint>
#endif

#ifdef HAVE_STDDEF_H
#   include <cstddef> /* ptrdiff_t */
#endif

#ifdef HAVE_SYS_TYPES_H
#   include <sys/types.h> /* off_t */
#endif

#ifdef HAVE_INTTYPES_H
#   include <cinttypes>
#endif


NS_BEGIN

#ifdef __cplusplus
extern "C" {
#endif

using int8    = std::int8_t;
using uint8   = std::uint8_t;
using byte    = std::uint8_t;
using int16   = std::int16_t;
using uint16  = std::uint16_t;
using int32   = std::int32_t;
using uint32  = std::uint32_t;
using int64   = std::int64_t;
using uint64  = std::uint64_t;
using intptr  = std::intptr_t;
using uintptr = std::uintptr_t;
using v8 = std::vector<byte>;
using u8 = uint8_t;


typedef struct _uint24
{
    uint8 b[3];
} uint24, uint24_be, uint24_le;

typedef int16  sint16, sint16_be, sint16_le;
typedef uint16         uint16_be, uint16_le;
typedef int32  sint32, sint32_be, sint32_le;
typedef uint32         uint32_be, uint32_le;
typedef int64  sint64, sint64_be, sint64_le;


#if FLOAT_LENGTH         == 8
typedef float       number64, number64_le, number64_be;
#elif DOUBLE_LENGTH      == 8
typedef double      number64, number64_le, number64_be;
#elif LONG_DOUBLE_LENGTH == 8
typedef long double number64, number64_le, number64_be;
#else
typedef uint64      number64, number64_le, number64_be;
#endif

#if defined(BYTE_ORDER)
#   undef KT_BIG_ENDIAN
#   if (BYTE_ORDER == BIG_ENDIAN)
#       define KT_BIG_ENDIAN 0x3210
#   endif
#endif



//
// 网络字节序到本地字节序
/**
 * convert big endian(network endian) 24 bits integers to native integers
 * @param x big endian value
 * @return native integers
 */
extern uint32   ntoh_uint24  (uint24   x);
extern int32    ntoh_uint24_3(u8 x, u8 y, u8 z);
extern number64 ntoh_number64(number64 x);
extern uint16   ntoh_uint16  (uint16   x);
extern int16    ntoh_sint16  (int16    x);
extern uint32   ntoh_uint32  (uint32   x);

//
// 本地字节序到网络字节序
extern uint24_be hton_uint32(uint32 x);
extern number64  hton_number64   (number64 x);

// other todo


//
// large file support
#ifdef HAVE_FSEEKO
#   undef  ftello
#   undef  fseeko
#   define ftello ftello
#   define fseeko fseeko
typedef off_t file_offset_t;
#endif//!HAVE_SEEKO


#ifdef __cplusplus
};
#endif

NS_END

#endif//INCLUDE_TYPES_H
