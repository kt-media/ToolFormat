#ifndef INCLUDE_FOUNDATION_H
#define INCLUDE_FOUNDATION_H

#ifndef NS_BEGIN
#define NS_BEGIN namespace kt {
#endif

#ifndef NS_END
#define NS_END                }
#endif

#ifndef NS_USING
#define NS_USING using namespace kt;
#endif

#endif//INCLUDE_FOUNDATION_H
