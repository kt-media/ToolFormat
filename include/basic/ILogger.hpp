#ifndef INCLUDE_I_LOGGER_H_
#define INCLUDE_I_LOGGER_H_

#include "Basic.hpp"
#include "Platform.hpp"
#include "Types.hpp"

NS_BEGIN

extern const uint32 MaxLogBufferLength;

enum LogLevel {
  DEBUG, INFO, WARN, ERROR
};

class ILogger {
 public:
  static ILogger &instance();

 public:
  virtual ~ILogger() = default;

  virtual void debug(const char *tag, const char *file, const char *func, int line, const char *format, ...) = 0;

  virtual void info(const char *tag, const char *file, const char *func, int line, const char *format, ...) = 0;

  virtual void warn(const char *tag, const char *file, const char *func, int line, const char *format, ...) = 0;

  virtual void error(const char *tag, const char *file, const char *func, int line, const char *format, ...) = 0;

 protected:
  ILogger() = default;
};

#ifndef    __FILE_NAME__
#   define __FILE_NAME__ __FILE__
#endif

#define logger_debug(tag, format, ...) \
    do {                               \
        ILogger::instance().debug(tag, __FILE_NAME__, __FUNCTION__, __LINE__, format, __VA_ARGS__); \
    }while(0)
#define logger_debug_m(tag, message) logger_debug(tag, message, 0)

#define logger_info(tag, format, ...)  \
    do {                               \
        ILogger::instance().info(tag, __FILE_NAME__, __FUNCTION__, __LINE__, format, __VA_ARGS__); \
    }while(0)
#define logger_info_m(tag, message) logger_info(tag, message, 0)

#define logger_warn(tag, format, ...)  \
    do {                               \
        ILogger::instance().warn(tag, __FILE_NAME__, __FUNCTION__, __LINE__, format, __VA_ARGS__); \
    }while(0)
#define logger_warn_m(tag, message) logger_warn(tag, message, 0)

#define logger_error(tag, format, ...)  \
    do {                               \
        ILogger::instance().error(tag, __FILE_NAME__, __FUNCTION__, __LINE__, format, __VA_ARGS__); \
    }while(0)
#define logger_error_m(tag, message) logger_error(tag, message, 0)

NS_END

#endif//INCLUDE_I_LOGGER_H_
