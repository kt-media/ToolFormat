#ifndef INCLUDE_PLATFORM_H_
#define INCLUDE_PLATFORM_H_

#include "Basic.hpp"

#define OS_MAC     0x100
#define OS_LINUX   0x200
#define OS_ANDROID 0x201
#define OS_WIN32   0x300

#ifdef WIN32
#   define OS_TYPE OS_WIN32
#elif defined(__linux__)
#   define OS_TYPE OS_LINUX
#elif defined(__APPLE__)
#   define OS_TYPE OS_MAC
#endif

#endif//INCLUDE_PLATFORM_H_
