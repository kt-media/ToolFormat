#ifndef _INCLUDE_AMF_H
#define _INCLUDE_AMF_H

#include "basic/Types.hpp"
#include <cstdlib>
#include <cstdio>

NS_BEGIN

#ifdef __cplusplus
extern "C" {
#endif


// amf data types
enum AMFDataTypes {
    AMF_TYPE_NUMBER             = byte(0x00),
    AMF_TYPE_BOOLEAN            = byte(0x01),
    AMF_TYPE_STRING             = byte(0x02),
    AMF_TYPE_OBJECT             = byte(0x03),
    AMF_TYPE_MOVIE_CLIP         = byte(0x04),  // 保留字段不使用
    AMF_TYPE_NULL               = byte(0x05),
    AMF_TYPE_UNDEFINED          = byte(0x06),
    AMF_TYPE_REFERENCE          = byte(0x07),
    AMF_TYPE_ECMA_ARRAY         = byte(0x08),
    AMF_TYPE_OBJECT_AND_MARKER  = byte(0x09), // 应该是结束??
    AMD_TYPE_STRICT_ARRAY       = byte(0x0A), // 严格数组
    AMF_TYPE_DATE               = byte(0x0B),
    AMF_TYPE_LONG_STRING        = byte(0x0C),

    AMF_TYPE_SIMPLE_OBJ         = byte(0x0D),
    AMF_TYPE_XML                = byte(0x0F),
    AMF_TYPE_CLASS              = byte(0x10),
};

// amf error code
enum AMFErrorCode {
    AMF_ERROR_OK                = byte(0x00),
    AMF_ERROR_EOF               = byte(0x01),
    AMF_ERROR_UNKNOWN_TYPE      = byte(0x02),
    AMF_ERROR_END_TAG           = byte(0x03),
    AMF_ERROR_NULL_POINTER      = byte(0x04),
    AMF_ERROR_MEMORY            = byte(0x05),
    AMF_ERROR_UNSUPPORTED_TYPE  = byte(0x06),
};


struct amf_data;
struct amf_string;
struct amf_list;
struct amf_node;

typedef struct amf_node *p_amf_node;


// string type
struct amf_string {
    uint16  size;
    byte   *mbstr;
};

// array type
struct amf_list {
    uint32     size;
    p_amf_node first_element;
    p_amf_node last_element;
};

// date type
struct amf_date {
    number64 milliseconds;
    int16    timezone;
};

// amf XML string type
struct amf_xml_string {
    uint32   size;
    byte    *mbstr;
};

// amf class type
struct amf_class {
    amf_string  name;
    amf_list    elements;
};

// structure encapsulating the various AMF objects
struct amf_data {
    byte   type;
    byte   error_code;
    union {
        number64       number_data;
        uint8          boolean_data;
        amf_string     string_data;
        amf_list       list_data;
        amf_date       date_data;
        amf_xml_string xml_data;
        amf_class      class_data;
    };
};

struct amf_node {
    amf_data  *data;
    p_amf_node prev;
    p_amf_node next;
};

// Pluggable backend support
typedef size_t (*amf_read_proc) (void       *out_buffer, size_t size, void *user_data);
typedef size_t (*amf_write_proc)(const void *in_buffer,  size_t size, void *user_data);


// read and write AMF data
amf_data *amf_data_read (                      amf_read_proc  read_proc,  void *user_data);
size_t    amf_data_write(const amf_data *data, amf_write_proc write_proc, void *user_data);


// allocate an AMF data object
amf_data *amf_data_new(byte type);

// load AMF data from buffer
amf_data *amf_data_buffer_read(byte *buffer, size_t max_bytes);
// load AMF data from stream
amf_data *amf_data_file_read(FILE *stream);

// AMF data size
size_t amf_data_size(const amf_data *data);

// write encoded AMF data into a buffer
size_t amf_data_buffer_write(amf_data *data, byte *buffer, size_t max_bytes);
// write encoded AMF data into a stream
size_t amf_data_file_write(const amf_data *data, FILE *stream);

// get the type of AMF data
byte amf_data_get_type(const amf_data *data);
// get the error code of AMF data
byte amf_data_get_error_code(const amf_data *data);
// return a new copy of AMF data */
amf_data *amf_data_clone(const amf_data *data);
//  release the memory of AMF data
void amf_data_free(amf_data *data);
// dump AMF data into a stream as text
void amf_data_dump(FILE *stream, const amf_data *data, int indent_level);

// return a null AMF object with the specified error code attached to it
amf_data *amf_data_error(byte error_code);

// number functions
amf_data *amf_number_new(number64 value);
number64  amf_number_get_value(const amf_data *data);
void      amf_number_set_value(amf_data *data, number64 value);

// boolean functions
amf_data *amf_boolean_new(uint8 value);
uint8     amf_boolean_get_value(const amf_data *data);
void      amf_boolean_set_value(amf_data *data, uint8 value);

// string functions
amf_data *amf_string_new(byte *str, uint16 size);
amf_data *amf_str(const char *str);
uint16    amf_string_get_size (const amf_data *data);
byte     *amf_string_get_bytes(const amf_data *data);

// object functions
amf_data *amf_object_new(void);
uint32    amf_object_size(const amf_data *data);
amf_data *amf_object_add(amf_data *data, const char *name, amf_data *element);
amf_data *amf_object_get(const amf_data *data, const char *name);
amf_data *amf_object_set(amf_data *data, const char *name, amf_data *element);
amf_data *amf_object_delete(amf_data *data, const char *name);
amf_node *amf_object_first(const amf_data *data);
amf_node *amf_object_last(const amf_data *data);
amf_node *amf_object_next(amf_node *node);
amf_node *amf_object_prev(amf_node *node);
amf_data *amf_object_get_name(amf_node *node);
amf_data *amf_object_get_data(amf_node *node);


// null functions
#define amf_null_new()      amf_data_new(AMF_TYPE_NULL)

// undefined functions
#define amf_undefined_new() amf_data_new(AMF_TYPE_UNDEFINED)

// associative array functions
amf_data *amf_associative_array_new(void);
#define amf_associative_array_size(d)       amf_object_size(d)
#define amf_associative_array_add(d, n, e)  amf_object_add(d, n, e)
#define amf_associative_array_get(d, n)     amf_object_get(d, n)
#define amf_associative_array_set(d, n, e)  amf_object_set(d, n, e)
#define amf_associative_array_delete(d, n)  amf_object_delete(d, n)
#define amf_associative_array_first(d)      amf_object_first(d)
#define amf_associative_array_last(d)       amf_object_last(d)
#define amf_associative_array_next(n)       amf_object_next(n)
#define amf_associative_array_prev(n)       amf_object_prev(n)
#define amf_associative_array_get_name(n)   amf_object_get_name(n)
#define amf_associative_array_get_data(n)   amf_object_get_data(n)

// array functions */
amf_data *amf_array_new(void);
uint32    amf_array_size(const amf_data *data);
amf_data *amf_array_push(amf_data *data, amf_data *element);
amf_data *amf_array_pop(amf_data *data);
amf_node *amf_array_first(const amf_data *data);
amf_node *amf_array_last(const amf_data *data);
amf_node *amf_array_next(amf_node *node);
amf_node *amf_array_prev(amf_node *node);
amf_data *amf_array_get(amf_node *node);
amf_data *amf_array_get_at(const amf_data *data, uint32 n);
amf_data *amf_array_delete(amf_data *data, amf_node *node);
amf_data *amf_array_insert_before(amf_data *data, amf_node *node, amf_data *element);
amf_data *amf_array_insert_after(amf_data *data, amf_node *node, amf_data *element);

// date functions
amf_data *amf_date_new(number64 milliseconds, uint16 timezone);
number64  amf_date_get_milliseconds(const amf_data *data);
uint16    amf_date_get_timezone    (const amf_data *data);
time_t    amf_date_to_time_t       (const amf_data *data);
size_t    amf_date_to_iso8601      (const amf_data *data, char *buffer, size_t buf_size);


#ifdef __cplusplus
};
#endif

NS_END

#endif//_INCLUDE_AMF_H
