#ifndef INCLUDE_FLV_H_
#define INCLUDE_FLV_H_

#include "basic/Basic.hpp"
#include "basic/Types.hpp"

#include <memory>
#include <string>
#include <vector>

NS_BEGIN

enum _flv_parse_error {
    FLV_PARSE_OK               = 0u,
    FLV_PARSE_NULL_POINTER     = 1u,
    FLV_PARSE_BAD_HEADER       = 2u,
    FLV_PARSE_EOF              = 3u,
    FLV_PARSE_NOT_FLV_FILE     = 4u,
    FLV_HAD_PARSED             = 5u,
    FLV_PARSE_BAD_TAG          = 6u,
    FLV_PROCESSING_UNSUPPORTED = 7u,
};

enum _magic_number {
    FLV_HEAD_DEFAULT_LEN  = 0x09u,
    FLV_TAG_BASIC_LEN     = 0x0Bu,
};

enum _flv_basic_tag_info {
    FLV_TAG_FILTER_NO_PRE_PROCESSING = 0x00u,
    FLV_TAG_FILTER_PRE_PROCESSING    = 0x20u,
    FLV_TAG_TYPE_AUDIO               = 0x08u,
    FLV_TAG_TYPE_VIDEO               = 0x09u,
    FLV_TAG_TYPE_SCRIPT_DATA         = 0x12u,
};

enum _flv_tag_audio_format {
    AUDIO_FORMAT_LINEAR_PCM_PE     = 0,  // Linear PCM, platform endian
    AUDIO_FORMAT_ADPCM             = 1,  // ADPCM
    AUDIO_FORMAT_MP3               = 2,  // MP3
    AUDIO_FORMAT_LINEAR_PCM_LE     = 3,  // Linear PCM, little endian
    AUDIO_FORMAT_NELLYMOSER_16K    = 4,  // Nellymoser 16-kHz mono
    AUDIO_FORMAT_NELLYMOSER_8K     = 5,  // Nellymoser 8-kHz mono
    AUDIO_FORMAT_NELLYMOSER        = 6,  // Nellymoser
    AUDIO_FORMAT_L_PCM_G711_LAW_A  = 7,  // G.711 A-law  logarithmic PCM
    AUDIO_FORMAT_L_PCM_G711_LAW_MU = 8,  // G.711 mu-law logarithmic PCM
    AUDIO_FORMAT_RESERVED          = 9,  // reserved
    AUDIO_FORMAT_AAC               = 10, // AAC
    AUDIO_FORMAT_SPEEX             = 11, // speex
    AUDIO_FORMAT_MP3_8K            = 14, // MP3 8-Khz
    AUDIO_FORMAT_DEVICE_S_S        = 15, // Device-specific sound
};

enum _flv_tag_audio_rate {
    AUDIO_RATE_KHZ_5_5 = 0u,     // 5.5Khz
    AUDIO_RATE_KHZ_11  = 1u,     //  11Khz
    AUDIO_RATE_KHZ_22  = 2u,     //  22Khz
    AUDIO_RATE_KHZ_44  = 3u,     //  44Khz(AAC格式总是该值)
};

enum _flv_tag_audio_size {
    AUDIO_SIZE_SAN_8_BIT  = 0u, // snd8Bit
    AUDIO_SIZE_SAN_16_BIT = 1u, // snd16Bit
};

enum _flv_tag_audio_type {
    AUDIO_TYPE_SND_MONO   = 0u, // sndMono   单声道
    AUDIO_TYPE_SND_STEREO = 1u, // sndStereo 立体声 (AAC 一定是该值)
};

enum _flv_tag_video_frame_type {
    VIDEO_FRAME_TYPE_KEY_FRAME         = 1u,  // key frame (for AVC, a seekable frame)
    VIDEO_FRAME_TYPE_INTER_FRAME       = 2u,  // inter frame (for AVC, a non-seekable frame)
    VIDEO_FRAME_TYPE_263_ONLY          = 3u,  // disposable inter frame (H.263 only)
    VIDEO_FRAME_TYPE_GENERATED_KEY_F   = 4u,  // generated key frame (reserved for server use only)
    VIDEO_FRAME_TYPE_COMMON_FRAME      = 5u,  // video info/command frame
};

enum _flv_tag_video_codec_identifier {
    VIDEO_CODEC_ID_263             = 2u, // Sorenson H.263
    VIDEO_CODEC_ID_SCREEN_VIDEO    = 3u, // Screen video
    VIDEO_CODEC_ID_VP6             = 4u, // On2 VP6
    VIDEO_CODEC_ID_VP6_ALPHA       = 5u, // On2 VP6 with alpha channel
    VIDEO_CODEC_ID_SCREEN_VIDEO_V2 = 6u, // Screen video version 2
    VIDEO_CODEC_ID_AVC             = 7u, // AVC
};

enum _flv_tag_avc_pkt_type {
    AVC_SEQUENCE_HEADER  = 0u, // AVC sequence header
    AVC_NALU             = 1u, // AVC NALU
    AVC_END_SEQUENCE     = 2u, // AVC end of sequence (lower level NALU sequence ender is not required or supported)
};


extern const char *audio_format_string(u8 format);
extern const char *audio_rate_string(u8 rate);
extern const char *audio_size_string(u8 size);
extern const char *audio_type_string(u8 type);

extern const char *video_frame_type_string(u8 type);
extern const char *video_codec_identifier_string(u8 codec_id);
extern const char *video_avc_packet_type_string(u8 type);


class FLVHeader;
class FLVParser;
class IOnTagParse;
class FLVTagHeader;

using BasicTagType  = byte;

class FLVHeader {
    friend class FLVParser;
    friend class IOnTagParse;
public:
    using onHeadExtra = uint8(*)(void *out, v8& extra);

private:
    uint8 readHeader(FILE *file);

protected:
    byte                signature[3] {};
    uint8               flags{0};
    uint8               version{0};
    uint32              length{0};     // 一般都是 9, > 9 时下面扩展内容起效果
    std::unique_ptr<v8> extra{};       // 头扩展内容,     一般用不到
    uint32              extra_len{0};  // 头扩展内容长度, 一般用不到

public:
    FLVHeader() = default;
    ~FLVHeader() = default;

    uint8 parseFLVHeadExtra(onHeadExtra cb, void *out);

public:
    [[nodiscard]] inline uint32 getLength() const;
    [[nodiscard]] inline uint8  getVersion() const;
    [[nodiscard]] inline bool   hasAudio() const;
    [[nodiscard]] inline bool   hasVideo() const;
};


class FLVTagHeader {
    friend class FLVParser;
    friend class IOnTagParse;
private:
    uint8 readTagHeader(FILE *file);

public:
    static uint8 readBasicTagType(FILE *file, BasicTagType *tagType);

protected:
    uint8   tag_type{0};            //
    uint24  body_length{0};         // in bytes, total tag size minus 11
    uint24  timestamp{0};           // milli-seconds
    uint8   timestamp_extended{0};  // timestamp extension
    uint24  stream_id{0};           // reserved, must be "\0\0\0"

public:
    [[nodiscard]] inline bool hasPreProcessing() const;
    [[nodiscard]] inline uint32 getBodyLength();
    [[nodiscard]] inline uint32 getTimestamp();
    [[nodiscard]] inline uint32 getStreamId();
    [[nodiscard]] inline uint8  getTagType() const;
};

class FLVTagAudio {
    friend class FLVParser;
    friend class IOnTagParse;
private:
    BasicTagType        audioType{0u};
    std::unique_ptr<v8> body{};
    uint32              length{0};
    FLVTagHeader&       tagHeader;

    uint8 parseAudioTag(FILE *file);

public:
    explicit FLVTagAudio(FLVTagHeader& header);
    ~FLVTagAudio() = default;

public:
    inline uint8 getSoundFormat();
    inline uint8 getSoundRate();
    inline uint8 getSoundSize();
    inline uint8 getSoundType();


    static inline uint8 audio_format(BasicTagType type);
    static inline uint8 audio_rate(BasicTagType type);
    static inline uint8 audio_size(BasicTagType type);
    static inline uint8 audio_type(BasicTagType type);
};

class FLVTagVideo {
    friend class FLVParser;
    friend class IOnTagParse;
private:
    BasicTagType        videoType{0u};
    std::unique_ptr<v8> body{};
    uint32              length{0};
    FLVTagHeader&       tagHeader;

    uint8 parseVideoTag(FILE *file);

public:
    explicit FLVTagVideo(FLVTagHeader& header);
    ~FLVTagVideo() = default;


    static inline u8 frame_type      (u8 type);
    static inline u8 codec_identifier(u8 codec_id);
};

class FLVTagScript {
    friend class FLVParser;
    friend class IOnTagParse;
private:
    std::unique_ptr<v8> body{};
    uint32              length{0};
    FLVTagHeader&       tagHeader;

    uint8 parseScriptTag(FILE *file);

public:
    explicit FLVTagScript(FLVTagHeader& header);
    ~FLVTagScript() = default;
};

class IOnTagParse {
public:
    IOnTagParse() = default;
    virtual ~IOnTagParse() = default;

    virtual bool onTagScript(v8& body) = 0;
    virtual bool onTagAudio(BasicTagType type, v8& body)   = 0;
    virtual bool onTagVideo(BasicTagType type, v8& body)   = 0;
    virtual bool onStreamEnd() = 0;
};


class FLVParser {
private:
    bool                         hasParsed;
    std::unique_ptr<std::string> ptrFilePath{};
    std::unique_ptr<FILE>        ptrFile{};

    void openFile();
    void closeFile();

private:
    static uint8 readPreviousTagSize(FILE *file, uint32 *value);

public:
    explicit FLVParser(const char *path);
    ~FLVParser();

    uint8 parser(IOnTagParse *listener = nullptr);
};

class OnTagParseDefault: public IOnTagParse {
public:
    OnTagParseDefault() = default;
    ~OnTagParseDefault() override = default;

    bool onTagScript(v8& body) override;
    bool onTagAudio(BasicTagType type, v8& body)  override;
    bool onTagVideo(BasicTagType type, v8& body)   override;
    bool onStreamEnd()override;
};




//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

uint32 FLVHeader::getLength() const
{
    return ntoh_uint32(length);
}

uint8 FLVHeader::getVersion() const
{
    return version;
}

bool FLVHeader::hasAudio() const
{
    return (flags & 0x04u) == 0x04u;
}

bool FLVHeader::hasVideo() const
{
    return (flags & 0x01u) == 0x01u;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

bool FLVTagHeader::hasPreProcessing() const
{
    return ( tag_type & FLV_TAG_FILTER_PRE_PROCESSING ) == FLV_TAG_FILTER_PRE_PROCESSING;
}

uint32 FLVTagHeader::getBodyLength()
{
    return ntoh_uint24(body_length);
}

uint32 FLVTagHeader::getTimestamp()
{
    return (timestamp_extended << 24 ) + ntoh_uint24(timestamp);
}

uint32 FLVTagHeader::getStreamId()
{
    return ntoh_uint24(stream_id);
}

uint8 FLVTagHeader::getTagType() const
{
    return ( tag_type & 0x1Fu );
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

uint8 FLVTagAudio::audio_format(BasicTagType type)
{
    return ( type & 0xF0u ) >> 4;

}

uint8 FLVTagAudio::audio_rate(BasicTagType type)
{
    return ( type & 0x0Cu ) >> 2;
}

uint8 FLVTagAudio::audio_size(BasicTagType type)
{
    return  (type & 0x02u) >> 1;
}

uint8 FLVTagAudio::audio_type(BasicTagType type)
{
    return type & 0x01;
}

uint8 FLVTagAudio::getSoundFormat()
{
    return audio_format(audioType);
}

uint8 FLVTagAudio::getSoundRate()
{
    return audio_rate(audioType);
}

uint8 FLVTagAudio::getSoundSize()
{
    return audio_size(audioType);
}

uint8 FLVTagAudio::getSoundType()
{
    return audio_type(audioType);
}

u8 FLVTagVideo::frame_type(u8 type)
{
    return (type & 0xF0) >> 4;
}

u8 FLVTagVideo::codec_identifier(u8 codec_id)
{
    return codec_id & 0x0F;
}

NS_END

#endif//INCLUDE_FLV_H_
